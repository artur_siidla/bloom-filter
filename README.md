# Bloom filter

Bloom filter implementation an described [here](http://codekata.com/kata/kata05-bloom-filters/).
## Building

First clone the repository to your desired location:

    git clone https://bitbucket.org/artur_siidla/bloom-filter.git

Then change directory to cloned project directory.

After that run ```mvn clean install``` to build the project

## Running

To run this Bloom filter implementation run this command (After following steps in Building section)

    mvn exec:java
    
## Testing

To test this Bloom filter implementation run this command (After following steps in Building section)

    mvn test

## NOTES

Main class is only to demonstrate this implementation of Bloom filter using required wordlist.
Tests and main class use same input file located in ```src/main/resources```

BloomFilter class itself resides in package ```com.artursiidla.bloomfilter.filter```

I used Google Guava library for hashing algorithms since I did not feel the need to reinvent the wheel.
In total five different hashing algorithms are used.

```maven-exec-plugin``` plugin is used to simplify running the application.
