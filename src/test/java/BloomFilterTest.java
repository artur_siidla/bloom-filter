import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.artursiidla.bloomfilter.filter.BloomFilter;

public class BloomFilterTest {

    /**
     * Every value that we bloom in our BloomFilter MUST exist within bloom filter.
     */
    @Test
    public void testAllMemberships() throws IOException {
        Set<String> words = getWords();
        BloomFilter bf = getBloomFilter(words, 0.00000001d);
        for (String word : words) {
            Assertions.assertTrue(bf.contains(word));
        }
    }

    /**
     * Testing false positive rate theory vs. practice.
     */
    @Test
    public void testFalsePositives() throws IOException {
        Set<String> words = getWords();
        double falsePositiveProbability = 0.0001d;
        BloomFilter bf = getBloomFilter(getWords(), falsePositiveProbability);
        // probability of a false positive should be ~1 in 10000 in this case
        int start = 10000, end = 20000;
        long falsePositiveCount = IntStream.range(start, end).mapToObj(n -> "element" + n).filter(bf::contains).count();
        int range = end - start;
        Assertions.assertTrue(falsePositiveCount < (words.size() + range) * falsePositiveProbability);
    }

    @Test
    public void testIllegalArguments() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BloomFilter(null, 0.1d));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BloomFilter(new HashSet<String>(Arrays.asList("1")), 2d));
    }

    private BloomFilter getBloomFilter(Set<String> words, double p) {
        return new BloomFilter(words, p);
    }

    private Set<String> getWords() throws IOException {
        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream("wordlist.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        Set<String> words = new HashSet<>();
        String line;
        while ((line = br.readLine()) != null) {
            words.add(line);
        }
        return words;
    }
}
