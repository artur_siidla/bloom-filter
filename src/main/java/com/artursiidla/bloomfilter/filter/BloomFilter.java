package com.artursiidla.bloomfilter.filter;

import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * Bloom filter implementation.
 * NOTE: Class is NOT Thread safe!
 * @author Artur Siidla
 */
public class BloomFilter {

    // in reality JVM doesn't exactly allocate a bit per boolean value (it's more like 8 bytes on 64bit system)
    // so it's kind of a waste but lets ignore that
    private boolean[] bitmap;
    private final Set<HashFunction> hashFunctions = new HashSet<>();

    /**
     * Initializes bloom filter with given values.
     * @param words {@link Set} of {@link String} values denoting values to hash
     * @param p desired false positive probability (must be a fraction between 0 and 1)
     */
    public BloomFilter(Set<String> words, double p) {
        if (words == null || words.isEmpty()) {
            throw new IllegalArgumentException("Words must not be null or empty!");
        }
        if (p <= 0 || p >= 1) {
            throw new IllegalArgumentException("False positive probability must must be a fraction between 0 and 1. (noninclusive)");
        }
        initHashFunctions();
        this.bitmap = new boolean[getOptimalBitmapSizeForProbability(words.size(), p).intValue()];
        fillBitmap(words);
    }

    /**
     * Calculates optimal size for bitmap as described in https://en.wikipedia.org/wiki/Bloom_filter
     * @param n number of keys
     * @param p desired false positive probability (must be a fraction between 0 and 1)
     * @return calculated optimal size for bitmap
     */
    public static Double getOptimalBitmapSizeForProbability(int n, double p) {
        return Math.abs((n * Math.log(p)) / Math.log(Math.pow(Math.log(2), 2)));
    }

    /**
     * Hashes {@link String} value and checks for bitmap collision.
     * Runs in O(1) since the number of hashing functions is constant in this implementation.
     * @param value {@link String} to check
     * @return true if value was found, false otherwise
     */
    public boolean contains(String value) {
        for (HashFunction hf : hashFunctions) {
            if (!bitmap[getIndex(value, hf)]) {
                return false;
            }
        }
        return true;
    }

    private void initHashFunctions() {
        hashFunctions.add(Hashing.sha256());
        hashFunctions.add(Hashing.adler32());
        hashFunctions.add(Hashing.murmur3_32());
        hashFunctions.add(Hashing.crc32());
        hashFunctions.add(Hashing.sipHash24());
    }

    /**
     * Runs in O(n) where n denotes the number of words. Number of hashes is constant so we don't account for that.
     * @param words {@link Set} of {@link String} values to bloom
     */
    private void fillBitmap(Set<String> words) {
        for (String word : words) {
            for (HashFunction hf : hashFunctions) {
                bitmap[getIndex(word, hf)] = true;
            }
        }
    }

    private int getIndex(String value, HashFunction hf) {
        // hashcode isn't perfect since it can (and often does) return a negative value
        return Math.abs(hf.hashString(value, StandardCharsets.UTF_8).toString().hashCode()) % bitmap.length;
    }
}
