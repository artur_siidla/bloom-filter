import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import com.artursiidla.bloomfilter.filter.BloomFilter;

public class Main {

    /**
     * Purpose of this main method is mostly to show example usage of {@link BloomFilter}
     * @param args NOP
     * @throws IOException in case file is not found
     */
    public static void main(String[] args) throws IOException {
        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream("wordlist.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        Set<String> words = new HashSet<>();
        String line;
        while ((line = br.readLine()) != null) {
            words.add(line);
        }
        BloomFilter bf = new BloomFilter(words, 0.0000001d);
        System.out.println(bf.contains(words.iterator().next()));
    }

}
